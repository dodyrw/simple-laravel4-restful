<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create("contact", function($table)
        {
            $table->engine = "InnoDB";

            $table->increments("id");
            $table->integer("user_id");

            $table->string("firstName");
            $table->string("lastName");
            $table->string("email");
            $table->string("address");
            $table->string("city");
            $table->string("postCode");
            $table->string("province");
            $table->string("country");
            $table->string("phone");
            $table->string("fax");

            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at");

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists("contact");
	}

}
